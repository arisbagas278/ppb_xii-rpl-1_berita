import 'package:flutter/material.dart';
import 'package:berita/ulangan/Berita.dart';
import 'package:berita/ulangan/Models/Details.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Berita',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
      home:  Berita(),
    );
  }
}
